package tfs.exceptions;

/**
 * Created by nofernan on 8/11/16.
 */
public class InvalidHeaderException extends RuntimeException {

    public InvalidHeaderException(String message){
        super(message);
    }

}
