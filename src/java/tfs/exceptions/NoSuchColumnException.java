package tfs.exceptions;

/**
 * Created by nofernan on 8/12/16.
 */
public class NoSuchColumnException extends RuntimeException {

    public NoSuchColumnException(String columnName){
        super(String.format("Column %s doesn't exists in the file", columnName));
    }

}
