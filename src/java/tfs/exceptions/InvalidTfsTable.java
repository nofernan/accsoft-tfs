package tfs.exceptions;

/**
 * Created by nofernan on 8/11/16.
 */
public class InvalidTfsTable extends RuntimeException {

    public InvalidTfsTable(String message){
        super(message);
    }
    public InvalidTfsTable(String message, Exception e){
        super(message, e);
    }

}
