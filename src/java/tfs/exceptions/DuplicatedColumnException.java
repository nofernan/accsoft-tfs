package tfs.exceptions;

/**
 * Created by nofernan on 8/12/16.
 */
public class DuplicatedColumnException extends RuntimeException {

    public DuplicatedColumnException(String columnName){
        super(String.format("The column %s already exists in the file", columnName));
    }

}
