package tfs.parsing;

/**
 * Created by nofernan on 8/11/16.
 */
public class TfsColumnDefinition {

    private Integer columnNumber;
    private String columnName;
    private TfsType columnType;

    public TfsColumnDefinition(Integer columnNumber, String columnName, TfsType columnType) {
        this.columnNumber = columnNumber;
        this.columnName = columnName;
        this.columnType = columnType;
    }



    public Integer getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(Integer columnNumber) {
        this.columnNumber = columnNumber;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public TfsType getColumnType() {
        return columnType;
    }

    public void setColumnType(TfsType columnType) {
        this.columnType = columnType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TfsColumnDefinition that = (TfsColumnDefinition) o;

        return columnName.equals(that.columnName);

    }

    @Override
    public int hashCode() {
        return columnName.hashCode();
    }

    @Override
    public String toString() {
        return "TfsColumnDefinition{" +
                "columnNumber=" + columnNumber +
                ", columnName='" + columnName + '\'' +
                ", columnType=" + columnType +
                '}';
    }
}
