package tfs.parsing;

import java.util.Map;

/**
 * Created by root on 2/16/17.
 */
public class TfsRow {

    private Integer rowNumber;
    private Map<Integer, TfsColumnData> columnData;

    public TfsRow() {
    }

    public TfsRow(Integer rowNumber, Map<Integer, TfsColumnData> columnData) {
        this.rowNumber = rowNumber;
        this.columnData = columnData;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Map<Integer, TfsColumnData> getColumnData() {
        return columnData;
    }

    public void setColumnData(Map<Integer, TfsColumnData> columnData) {
        this.columnData = columnData;
    }
}
