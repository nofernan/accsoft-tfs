package tfs.parsing;

/**
 * Created by nofernan on 8/11/16.
 */
public class TfsColumnData {

    private TfsColumnDefinition associatedColumn;
    private Integer row;
    private Object content;

    public TfsColumnData(TfsColumnDefinition associatedColumn, Integer row, Object content) {
        this.associatedColumn = associatedColumn;
        this.row = row;
        this.content = content;
    }


    public TfsColumnDefinition getAssociatedColumn() {
        return associatedColumn;
    }

    public void setAssociatedColumn(TfsColumnDefinition associatedColumn) {
        this.associatedColumn = associatedColumn;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Object getValue() {
        return content;
    }

    public void setValue(Object content) {
        this.content = content;
    }

    public Double getDoubleValue(){
        return Double.parseDouble(this.content.toString());
    }
    public Integer getIntegerValue(){
        return Integer.parseInt(this.content.toString());
    }

    public String getString(){
        return this.content.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TfsColumnData that = (TfsColumnData) o;

        if (associatedColumn != null ? !associatedColumn.equals(that.associatedColumn) : that.associatedColumn != null)
            return false;
        if (row != null ? !row.equals(that.row) : that.row != null) return false;
        return content != null ? content.toString().equals(that.content.toString()) : that.content == null;

    }

    @Override
    public int hashCode() {
        int result = associatedColumn != null ? associatedColumn.hashCode() : 0;
        result = 31 * result + (row != null ? row.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TfsColumnData{" +
                "associatedColumn=" + associatedColumn +
                ", row=" + row +
                ", content=" + content +
                '}';
    }
}
