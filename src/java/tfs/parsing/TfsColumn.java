package tfs.parsing;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nofernan on 8/11/16.
 */
public class TfsColumn {

    private Collection<TfsColumnData> data;
    private TfsColumnDefinition definition;

    public TfsColumn(TfsColumnDefinition definition, Collection<TfsColumnData> data) {
        this.data = data;
        this.definition = definition;
    }

    public TfsColumnDefinition getDefinition() {
        return definition;
    }

    public TfsColumnData getRow(Integer rowNumber) {
        return data.stream().filter(row -> row.getRow().equals(rowNumber)).findAny().get();
    }

    public List<TfsColumnData> getRows() {
        return data.stream().sorted((row1, row2) -> row1.getRow().compareTo(row1.getRow())).collect(Collectors.toList());
    }
}
