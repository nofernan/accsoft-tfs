package tfs.parsing;

import java.util.Arrays;
import java.util.stream.Collectors;

import tfs.exceptions.InvalidHeaderException;

/**
 * Created by nofernan on 8/11/16.
 */
public class TfsHeader {


    private final String headerName;
    private final TfsType type;
    private final Object value;

    public TfsHeader(String headerName, TfsType type, Object value){

        this.headerName = headerName;
        this.type = type;
        this.value = value;
    }




    public String getName() {
        return headerName;
    }

    public TfsType getType() {
        return type;
    }

    public String getStringValue() {
        return value.toString();
    }


    public static TfsHeader parse(String text) {

        if (text.matches("@\\s*\\w+\\s+%(le|s|d|[0-9])+\\s+.+")) {
            String[] parsedText = text.split("\\s+");

            TfsType headerType = null;

            if(parsedText[1].contains("le")){
                headerType = TfsType.LE;
            }else if(parsedText[1].contains("s")){
                headerType = TfsType.S;
            }else{
                headerType = TfsType.D;
            }

            return new TfsHeader(parsedText[0].replace("@", ""), headerType, Arrays.asList(parsedText).subList(2, parsedText.length).stream().collect(Collectors.joining(" ")));
        } else {
            throw new InvalidHeaderException(String.format("Header not properly formatted: %s", text));
        }
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("@%s", headerName));
        sb.append("     ");
        sb.append("%" + type.name().toLowerCase());
        sb.append("     ");
        sb.append(String.format("%s", value));
        sb.append("     ");

        return sb.toString();
    }



}
