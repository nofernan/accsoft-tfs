package tfs.parsing;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import tfs.exceptions.DuplicatedColumnException;
import tfs.exceptions.NoSuchColumnException;

/**
 * Created by nofernan on 8/11/16.
 */
public class TfsFile {

    private List<TfsHeader> headers;

    public void setColumns(Map<Integer, TfsColumnDefinition> columns) {
        this.columns = columns;
    }

    private Map<Integer, TfsColumnDefinition> columns;
    private List<String> rawData;

    public TfsFile() {
        this.headers = new ArrayList<>();
        this.columns = new HashMap<>();
    }

    public void setHeaders(List<TfsHeader> headers) {
        this.headers = headers;
    }

    public void addHeader(TfsHeader header) {
        this.headers.add(header);
    }

    public List<TfsHeader> getHeaders() {
        return headers;
    }

    public Map<Integer, TfsColumnDefinition> getColumns() {
        return columns;
    }


    public Stream<Map<String, TfsColumnData>> getRows() {
        return rawData.stream().map(line -> {
            List<String> row = parseLine(line);
            return row.stream()
                    .map(column -> new TfsColumnData(columns.get(row.indexOf(column)), rawData.indexOf(line), column))
                    .collect(Collectors.toMap(col -> col.getAssociatedColumn().getColumnName(), col -> col));
        });
    }

    public Stream<TfsColumnData> getColumn(Integer columnNumber) {
        Optional<TfsColumnDefinition> foundData = Optional.ofNullable(this.columns.get(columnNumber));

        if (foundData.isPresent()) {
            TfsColumnDefinition columnDef = foundData.get();
            return rawData.stream().map(line -> new TfsColumnData(columnDef, rawData.indexOf(line), parseLine(line).get(columnNumber)));
        } else {
            throw new IllegalArgumentException(String.format("The index %d is out of file range [0, %d]", columnNumber, columns.size()));
        }
    }


    public Stream<TfsColumnData> getColumn(String columnName) {
        Optional<TfsColumnDefinition> foundData = this.columns.values()
                .stream()
                .filter(column -> column.getColumnName().equals(columnName))
                .findFirst();

        if (foundData.isPresent()) {
            return getColumn(foundData.get().getColumnNumber());
        } else {
            throw new NoSuchColumnException(columnName);
        }
    }

    private List<String> parseLine(String line) {
        return Arrays.stream(line.split("\\s+"))
                .filter(row -> !row.isEmpty())
                .collect(Collectors.toList());
    }


    public boolean columnExists(String columnName) {

        try {
            getColumn(columnName);
            return true;
        } catch (NoSuchColumnException e) {
            return false;
        }

    }


    private boolean checkColumnType(TfsType columnType, Object value) {

        if (columnType.equals(TfsType.LE)) {
            // is a double value

            try {
                Double.parseDouble(value.toString());
            } catch (NumberFormatException e) {
                return false;
            }
            return true;
        } else if (columnType.equals(TfsType.S)) {
            // string
            return true;        // everything could be a string

        } else {
            try {
                Integer.parseInt(value.toString());
                return true;
            } catch (NumberFormatException e) {
                return false;
            }

        }

    }


    public Stream<Object> getRow(Integer rowNumber) {
        return rawData.stream().map(line -> parseLine(line).get(rowNumber));
    }

    public Integer getNumberOfRows() {
        return rawData.size();     // I assume all the columns has the same number of elements
    }


    public void setRawData(List<String> rawData) {
        this.rawData = rawData;
    }
}
