package tfs.parsing;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import tfs.exceptions.InvalidHeaderException;
import tfs.exceptions.InvalidTfsTable;

import javax.swing.plaf.multi.MultiMenuBarUI;

/**
 * Created by nofernan on 8/11/16.
 */
public class TfsReader {

    public static TfsFile parseString(List<String> fileContentSplittedInLines, ParsingMode... parsingModes) {

        List<ParsingMode> parsingModeList = Arrays.asList(parsingModes);

        try {
            TfsFile parsedFile = new TfsFile();

            List<TfsHeader> headers = getTfsHeaders(fileContentSplittedInLines, parsingModeList);
            List<String> data = fileContentSplittedInLines.stream().skip(headers.size()).collect(Collectors.toList());
            Map<Integer, TfsColumnDefinition> columnDefinitions = getColumnDefinitions(fileContentSplittedInLines).stream().collect(Collectors.toMap(TfsColumnDefinition::getColumnNumber, t -> t));

            parsedFile.setHeaders(headers);
            parsedFile.setColumns(columnDefinitions);
            parsedFile.setRawData(data.stream().skip(2).collect(Collectors.toList()));
            return parsedFile;
        } catch (InvalidHeaderException e) {
            throw new InvalidTfsTable("Error found when parsing the content", e);
        }

    }

    /**
     * Instead of rows, this works with columns
     *
     * @param fileContentSplittedInLines
     * @return
     */
    private static Map<TfsColumnDefinition, List<TfsColumnData>> getColumnsEfficient(List<TfsHeader> headers, List<String> fileContentSplittedInLines) {

        Multimap<TfsColumnDefinition, List<TfsColumnData>> foundColumns = LinkedListMultimap.create();
        List<TfsColumnDefinition> columnDefinitions = getColumnDefinitions(fileContentSplittedInLines);


        return null;
    }


    private static Map<TfsColumnDefinition, Collection<TfsColumnData>> getColumns(List<String> fileContentSplittedInLines, List<ParsingMode> parsingModeList) {

        Multimap<TfsColumnDefinition, TfsColumnData> dataToReturn = ArrayListMultimap.create();

        Map<TfsColumnDefinition, List<TfsColumnData>> foundColumns = new HashMap<>();
        Map<Integer, TfsColumnDefinition> columnDefinitions = getColumnDefinitions(fileContentSplittedInLines).stream().collect(Collectors.toMap(TfsColumnDefinition::getColumnNumber, t -> t));

        //columnDefinitions.forEach(columnDefinition -> foundColumns.put(columnDefinition, new ArrayList<>()));


        List<String> data = fileContentSplittedInLines.stream().skip(2).collect(Collectors.toList());


        try (Stream<String> rowData = data.stream()) {
            rowData.forEach(line -> {

                List<String> splittedROw = Arrays.stream(line.split("\\s+"))
                        .filter(row -> !row.isEmpty())
                        .collect(Collectors.toList());

                splittedROw.forEach(dataForColumn -> {
                    TfsColumnDefinition def = columnDefinitions.get(splittedROw.indexOf(dataForColumn));
                    dataToReturn.put(def, new TfsColumnData(def, 1, dataForColumn));
                });
            });
        }

        return dataToReturn.asMap();

    }

    private static List<String> getData(List<String> fileContentSplittedInLines) {
        return fileContentSplittedInLines.stream()
                .filter(line -> !line.isEmpty() && !isColumnNameRrow(line) && !isColumnTypeRow(line) && !isHeaderRow(line))
                .collect(Collectors.toList());
    }

    private static boolean isHeaderRow(String line) {
        return line.startsWith("@");
    }

    private static List<TfsColumnDefinition> getColumnDefinitions(List<String> fileContentSplittedInLines) {
        Optional<String> columnNames = fileContentSplittedInLines.stream().filter(TfsReader::isColumnNameRrow).findAny();
        Optional<String> columnTypes = fileContentSplittedInLines.stream().filter(TfsReader::isColumnTypeRow).findAny();

        Map<Integer, TfsType> typesAndColumnNumber = new HashMap<>();
        if (columnTypes.isPresent()) {
            List<String> columns = Arrays.asList(columnTypes.get().replaceFirst("\\$\\s+", "").split("\\s+"));

            for (int i = 0; i < columns.size(); i++) {
                typesAndColumnNumber.put(i, TfsType.valueOf(columns.get(i).replace("%", "").toUpperCase()));
            }

        } else {
            throw new InvalidTfsTable("Row with column types is not present");
        }

        if (columnNames.isPresent()) {
            List<String> columns = Arrays.asList(columnNames.get().replaceFirst("\\*\\s+", "").split("\\s+"));

            if (columns.size() != typesAndColumnNumber.size()) {
                throw new InvalidTfsTable("There is a discrepancy between the amount of column names and column types");
            }

            return columns.stream()
                    .map(column -> new TfsColumnDefinition(columns.indexOf(column), column, typesAndColumnNumber.get(columns.indexOf(column)))).
                            collect(Collectors.toList());
        } else {
            throw new InvalidTfsTable("Row with column names is not present");
        }
    }

    private static boolean isColumnTypeRow(String line) {
        return line.matches("\\$\\s+(%\\w+\\s*)+");
    }

    private static boolean isColumnNameRrow(String line) {
        return line.matches("\\*\\s+(\\w+\\s+)*\\w+\\s*");
    }

    private static List<TfsHeader> getTfsHeaders(List<String> fileContentSplittedInLines, List<ParsingMode> parsingModeList) {

        List<TfsHeader> headers = new ArrayList<>();

        for (String line : fileContentSplittedInLines.stream().filter(row -> row.startsWith("@")).collect(Collectors.toList())) {
            try {
                headers.add(TfsHeader.parse(line));
            } catch (InvalidHeaderException e) {
                if (!parsingModeList.contains(ParsingMode.NO_HEADER_CHECK)) {
                    throw e;
                } else {
                    System.out.println(String.format("Error found when parsing header %s, skipping...", line));
                }
            }
        }


        if (!parsingModeList.contains(ParsingMode.NO_HEADER_CHECK) && headers.size() == 0) {
            throw new InvalidTfsTable("No headers were found on the file");
        }

        return headers;

    }

    public static TfsFile parse(File testColumnNameRowParsing, ParsingMode... parsingModes) throws IOException {
        List<String> fileContentSplittedInLines = Files.readAllLines(testColumnNameRowParsing.toPath());
        return parseString(fileContentSplittedInLines, parsingModes);
    }
}
