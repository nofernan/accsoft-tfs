package tfs.IO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import tfs.parsing.TfsFile;

/**
 * Created by noe on 14/08/16.
 */
public class TfsFileWritter {

    /**public static void write(TfsFile fileToSave, File destination) throws IOException {

       try(FileWriter fw = new FileWriter(destination)){

           fileToSave.getHeaders().forEach(header -> writeLine(fw, header));


           fw.write("* ");
           fileToSave.getColumnDefinitions().stream().map(def -> def.getColumnName()).forEach(colName -> {
               try {
                   fw.write(String.format("%s\t", colName));
               } catch (IOException e) {
                   e.printStackTrace();
               }
           });


           fw.write("\n");
           fw.write("$ ");

           fileToSave.getColumnDefinitions().stream().map(column -> column.getColumnType()).forEach(colType -> {
               try {
                   fw.write(String.format("%s    ", "%" + colType.name().toLowerCase()));
               } catch (IOException e) {
                   e.printStackTrace();
               }
           });

           fw.write("\n");

           for(List<Object> row : fileToSave.getRows()){
               for(Object data  : row){
                   fw.write(String.format("%s    ", data));
               }

               fw.write("\n");
           }



       }catch (IOException e){
           throw e;
       }

    }

    private static void writeLine(FileWriter fw, Object header){
        try {
            fw.write(String.format("%s\n", header));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }**/
}
