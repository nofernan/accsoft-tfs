package tests.tfs.loading;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;

import tfs.exceptions.NoSuchColumnException;
import tfs.parsing.ParsingMode;
import tfs.parsing.TfsColumnData;
import tfs.parsing.TfsFile;
import tfs.parsing.TfsReader;

/**
 * Created by nofernan on 8/11/16.
 */
public class TestTwissLHCFile {


    @Test
    public void loadOriginalWiseFile() throws IOException, InterruptedException {
        Long file1 = System.currentTimeMillis();
        TfsFile file = TfsReader.parse(new File("testresources/bigfiles/SQUEEZE-6.5TeV-3m-40cm-2016_V1--B1--0.0--0.tfs"), ParsingMode.NO_HEADER_CHECK);
        Long file2 = System.currentTimeMillis();
        System.out.println(String.format("THe whole thing  took %d", (file2 - file1)));

        // ACCESS SPECIFIC COLUMN
        file.getColumn("NAME").forEach(data -> {
            System.out.println(String.format("%4d - %15s", data.getRow(), data.getString()));
        });



        // ALL THE COLUMNS

        file.getRows().forEach(row -> {
            row.forEach((columnName, data) -> {
                System.out.println(columnName + "\t" + data.getString());
            });
        });


    }

    @Test
    public void getColumnByName() throws IOException {
        TfsFile wiseFile = TfsReader.parse(new File("testresources/bigfiles/SQUEEZE-6.5TeV-3m-40cm-2016_V1--B1--0.0--0.tfs"), ParsingMode.NO_HEADER_CHECK);
        List<TfsColumnData> mdx = wiseFile.getColumn("M_DX").collect(Collectors.toList());

        assertEquals(128, mdx.size());

        assertEquals(new Double(0.00313), mdx.get(0).getDoubleValue());
        assertEquals(new Double(-0.004865), mdx.get(3).getDoubleValue());
        assertEquals(new Double(-0.00384), mdx.get(17).getDoubleValue());


        List<TfsColumnData> mdxByNumber = wiseFile.getColumn(2).collect(Collectors.toList());

        assertEquals(128, mdxByNumber.size());

        assertEquals(new Double(0.00313), mdxByNumber.get(0).getDoubleValue());
        assertEquals(new Double(-0.004865), mdxByNumber.get(3).getDoubleValue());
        assertEquals(new Double(-0.00384), mdxByNumber.get(17).getDoubleValue());

    }


    @Test(expected = NoSuchColumnException.class)
    public void getMissingColumnByName() throws IOException {
        TfsFile wiseFile = TfsReader.parse(new File("testresources/bigfiles/extractor_wise_geo_tfs.tfs"), ParsingMode.NO_HEADER_CHECK);
        List<TfsColumnData> mdx = wiseFile.getColumn("I_DONT_EXIST").collect(Collectors.toList());
    }


    @Test(expected = IllegalArgumentException.class)
    public void getMissingColumnByNumber() throws IOException {
        TfsFile wiseFile = TfsReader.parse(new File("testresources/bigfiles/extractor_wise_geo_tfs.tfs"), ParsingMode.NO_HEADER_CHECK);
        List<TfsColumnData> mdx = wiseFile.getColumn(200).collect(Collectors.toList());
    }

    @Test
    public void splitt() {
        String toSplit = " \"asdas\" 2342    23542";
        System.out.println(Arrays.stream(toSplit.split("\\s+")).filter(l -> !l.isEmpty()).collect(Collectors.toList()));
    }


}
