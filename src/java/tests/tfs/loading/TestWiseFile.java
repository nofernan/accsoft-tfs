package tests.tfs.loading;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import tfs.exceptions.NoSuchColumnException;
import tfs.parsing.ParsingMode;
import tfs.parsing.TfsColumnData;
import tfs.parsing.TfsFile;
import tfs.parsing.TfsReader;


public class TestWiseFile {


    @Test
    public void loadOriginalWiseFile() throws IOException {
        Long time1 = System.currentTimeMillis();
        File fileToRead = new File("testresources/bigfiles/extractor_wise_geo_tfs.tfs");
        TfsFile file = TfsReader.parse(fileToRead, ParsingMode.NO_HEADER_CHECK);
        Long time2 = System.currentTimeMillis(); // after


        System.out.println(String.format("Took %d ms to read %d kbytes", (time2 - time1), fileToRead.length()));
    }

    @Test
    public void getColumnByName() throws IOException {
      TfsFile wiseFile = TfsReader.parse(new File("testresources/bigfiles/extractor_wise_geo_tfs.tfs"), ParsingMode.NO_HEADER_CHECK);
        List<TfsColumnData> mdx = wiseFile.getColumn("M_DX").collect(Collectors.toList());



        assertEquals(128, mdx.size());

        assertEquals(new Double(0.00313), mdx.get(0).getDoubleValue());
        assertEquals(new Double(-0.004865), mdx.get(3).getDoubleValue());
        assertEquals(new Double(-0.00384), mdx.get(17).getDoubleValue());


        List<TfsColumnData> mdxByNumber = wiseFile.getColumn(2).collect(Collectors.toList());

        assertEquals(128, mdxByNumber.size());

        assertEquals(new Double(0.00313), mdxByNumber.get(0).getDoubleValue());
        assertEquals(new Double(-0.004865), mdxByNumber.get(3).getDoubleValue());
        assertEquals(new Double(-0.00384), mdxByNumber.get(17).getDoubleValue());

    }


    @Test(expected = NoSuchColumnException.class)
    public void getMissingColumnByName() throws IOException {
        TfsFile wiseFile = TfsReader.parse(new File("testresources/bigfiles/extractor_wise_geo_tfs.tfs"), ParsingMode.NO_HEADER_CHECK);
        List<TfsColumnData> mdx = wiseFile.getColumn("I_DONT_EXIST").collect(Collectors.toList());
    }


    @Test(expected = IllegalArgumentException.class)
    public void getMissingColumnByNumber() throws IOException {
        TfsFile wiseFile = TfsReader.parse(new File("testresources/bigfiles/extractor_wise_geo_tfs.tfs"), ParsingMode.NO_HEADER_CHECK);
        List<TfsColumnData> mdx = wiseFile.getColumn(200).collect(Collectors.toList());
    }



}
