package tests.tfs.parsing;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import tfs.exceptions.InvalidTfsTable;
import tfs.parsing.ParsingMode;
import tfs.parsing.TfsFile;
import tfs.parsing.TfsHeader;
import tfs.parsing.TfsReader;
import tfs.parsing.TfsType;

/**
 * Created by nofernan on 8/11/16.
 */
public class HeaderLoadingTests {


    @Test
    public void loadSimpleHeaders(){
        List<String> content = Arrays.asList("@header1 %le blabla1","@header2 %le blabla2");
        List<TfsHeader> headers = content.stream().map(line -> TfsHeader.parse(line)).collect(Collectors.toList());

        assertEquals(2, headers.size());

        assertEquals(headers.get(0).getName(), "header1");
        assertEquals(headers.get(0).getType(), TfsType.LE);
        assertEquals(headers.get(0).getStringValue(), "blabla1");

        assertEquals(headers.get(1).getName(), "header2");
        assertEquals(headers.get(1).getType(), TfsType.LE);
        assertEquals(headers.get(1).getStringValue(), "blabla2");

    }

    @Test(expected = InvalidTfsTable.class)
    public void loadNoHeaders(){
        List<String> fileContent = new ArrayList<>();
        TfsFile parsedTfsFile = TfsReader.parseString(fileContent);

    }



    @Test
    public void loadWrongHeadersSkippingCheck() throws IOException {
        TfsFile parsedTfsFile = TfsReader.parse(new File("testresources/loadWrongHeadersSkippingCheck.tfs"), ParsingMode.NO_HEADER_CHECK);

        assertEquals(0, parsedTfsFile.getHeaders().size());

    }
}
