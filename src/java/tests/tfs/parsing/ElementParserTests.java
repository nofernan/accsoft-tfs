package tests.tfs.parsing;

import static junit.framework.TestCase.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import tfs.exceptions.InvalidHeaderException;
import tfs.exceptions.InvalidTfsTable;
import tfs.parsing.TfsFile;
import tfs.parsing.TfsHeader;
import tfs.parsing.TfsReader;
import tfs.parsing.TfsType;

/**
 * Tests the parsing constraints on the TFS tables
 * Created by nofernan on 8/11/16.
 */
public class ElementParserTests {


    @Test
    public void testHeader(){
        String headerText = "@nameheader    %09s   value";
        TfsHeader header = TfsHeader.parse(headerText);

        assertEquals("nameheader", header.getName());
        assertEquals(TfsType.S, header.getType());
        assertEquals("value", header.getStringValue());
    }


    @Test
    public void testHeaderParsingValueWithSpaces(){
        String headerText = "@nameheader    %le   \"val ue a\"";
        TfsHeader header = TfsHeader.parse(headerText);

        assertEquals("nameheader", header.getName());
        assertEquals(TfsType.LE, header.getType());
        assertEquals("\"val ue a\"", header.getStringValue());
    }


    @Test(expected = InvalidHeaderException.class)
    public void testInvalidHeaderWithoutName(){
        String headerText = "@    %datatype   \"val ue a\"";
        TfsHeader.parse(headerText);
    }

    @Test
    public void testHeaderNameWordsAndNumbers(){
        String headerText = "@header1   %le   \"val ue a\"";
        TfsHeader header = TfsHeader.parse(headerText);

        assertEquals("header1", header.getName());
    }



    @Test(expected = InvalidTfsTable.class)
    public void testTfsFileWithoutColumnNameRow() throws IOException {
        TfsReader.parse(new File("testresources/loadNoColumnNameHeader.tfs"));
    }

    @Test(expected = InvalidTfsTable.class)
    public void testTfsFileWithoutColumnTypeRow() throws IOException {
        TfsReader.parse(new File("testresources/loadNoColumnTypeHeader.tfs"));
    }


    @Test
    public void parseLine(){
        String line = "UNO        DOS\t\tTRES\t\t 0.1   2   -3.145";

        String[] as = line.split("\\s+");

        assertEquals(6, as.length);

        assertEquals("UNO", as[0]);
        assertEquals("DOS", as[1]);
        assertEquals("TRES", as[2]);

    }

    @Test
    public void parseOriginalLine(){
        String originalLine = "\"MQXA.1R1\"                    26.150005                     0.00313                       -0.00101                      -1.1E-4                       1.002E-5                      0.0                           0.0                           0.0                           -0.275                        -1.24                         4.09                          0.01                          -0.00830564784053156          0.002373042240151881          0                             0                             180                           102104                        LQXAA.1R1                     \n";
        String[] as = originalLine.split("\\s+");

        assertEquals(20, as.length);

    }

}
